# Spotify to ListenBrainz History Import

This is a small script that can import your old Spotify history to ListenBrainz.
## Usage
- Go to [https://www.spotify.com/us/account/privacy/](https://www.spotify.com/us/account/privacy/) and request your listening history.
- To avoid duplicates, you should edit the `json` files to remove any entries which you have already submitted. You can use
    ```sh
    cat Streaming_History_Audio_2024_20.json | jq > path/to/history_2024_20.readable.json
    ```
    to get the file into a human readable form and then edit it.
- Go to [https://listenbrainz.org/profile/](https://listenbrainz.org/profile/) and copy your user token.
- Run the script with the user token and the (edited) `json` files as arguments.
    ```sh
    ./import-from-spotify.py --lb-token <token> FILES...
    ```
    Optional arguments are
    ```txt
    -n, --dry-run		Do not upload anything to ListenBrainz. Useful for making sure there
                            are no errors before uploading.
    --min-date <DATE>	Skip all entries where the UTC timestamp is smaller than <DATE>
                            (in ISO 8601 format)
    --max-date <DATE>	Skip all entries where the UTC timestamp is bigger than <DATE>
                            (in ISO 8601 format)
    --lbexport <FILE>	Avoid submitting duplicates by checking against the ListenBrainz export from
                            https://listenbrainz.org/profile/export/
			        Only detects duplicates from submissions that were uploaded with this script!
    ```

    There are a few safety checks to make sure that you do not import bullshit.
    - You will be shown the first valid entry with all its data.
    - The first valid entry is set to "Listening now".
    - The first 10 entries are imported and you are given a link to view them in your history.
        If something went wrong you can easily manually delete these 10 entries via the browser interface.

    After this there are no further checks. All the entries of all the files are imported until we are finished or an error occurs.

- In the end you will see a summary
    ```txt
    Total elements in json files: 12345
    Uploaded: 9876
    Rejected, because of short duration: 2444
    Skipped, because the timestamp was not in our date range: 0
    Skipped, because of missing key: 20
    Skipped, because we could not find the duration: 2
    Skipped, because of errors: 3
    ```
    - "Short duration" means that you listened to the track for less than 4 minutes and also for less than half the track's duration.
    - Our date range is specified with the optional arguments `--min-date` and `--max-date`.
    - The entries with a missing key are probably podcast episodes. They are not logged.
    - "Could not find the duration" are pobably HTML errors when requesting the spotify website.
        These entries are saved into the `not_imported.json` and you should probably rename
        this file and run the script again with it as an argument.
    - The last category are general errors and you should inspect `not_imported.json` to see what might
        be wrong with the entries.

## What It Does
The script goes through all the entries in the supplied files.
For each entry it looks at the Spotify website to get the total duration of the song.
This is the slowest part of the script.
I also expect this mechanism to break as soon as Spotify changes their web interface, but hey, at least I did not need to use their API.
To reduce the necessary number of HTML requests, the song's durations are cached to the file `duration_cache.pickle`.

After getting the duration, the script looks at the `ms_played` field in the entry and compares it to the total duration of the song.
As per the [official ListenBrainz documentation](https://listenbrainz.readthedocs.io/en/latest/users/api/core.html?post--1-submit-listens#post--1-submit-listens)
a song is only submitted if it was played for over 4 minutes, or at least half the song's duration.

All the entries that fullfil this criterion are imported in batches of 1000
(the [maximum allowed number](https://listenbrainz.readthedocs.io/en/latest/users/api/core.html#listenbrainz.webserver.views.api_tools.MAX_LISTENS_PER_REQUEST))
with their correct timestamp from when they were listened to.

## Can It Go Any Faster?
Yes, if you use the Spotify API to get the durations then it only takes 0.4 times as long.
- Go to [https://developer.spotify.com/dashboard](https://developer.spotify.com/dashboard)
- Create a new app and go to its settings
- Copy the client id and secret
- Put them into a file `spotify.txt` where the first line is the id and the second one is the secret, like this
    ```txt
    a52456b21059d01240
    a51121ab214582c100
    ```
The script should detect this file and give feedback
"Successfully requested Spotify API Token."

Expect to get rate limited by Spotify at which point the script will use web scraping again for a while.
