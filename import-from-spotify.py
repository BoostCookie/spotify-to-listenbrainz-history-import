#!/usr/bin/env python3
from datetime import datetime, timezone, timedelta
from itertools import islice
from typing import Optional
import base64
import json
import os
import pickle
import requests
import sys
import time

LB_API_URL = "https://api.listenbrainz.org"
MIN_DATE_TS = 1033430400
MIN_DATE = datetime.fromtimestamp(MIN_DATE_TS, tz=timezone.utc)
current_file_spotify_listen_counter = 0
duration_not_found_counter = 0
rejected_for_duration_counter = 0
skipped_because_of_error_counter = 0
skipped_because_of_date_counter = 0
skipped_because_of_missing_key_counter = 0
skipped_because_of_duplicate_counter = 0
spotify_listen_counter = 0
not_imported_path = "not_imported.json"
duration_cache_path = "duration_cache.pickle"
stop_early = False
spotify_auth_header = None
pause_spotify_api_until = datetime.now(tz=timezone.utc)

def spotify_track_url(track_uri: str) -> str:
    return "https://open.spotify.com/track/{}".format(track_uri.rsplit(":", maxsplit=1)[1])

def set_spotify_api_auth_header():
    global spotify_auth_header
    if not os.path.isfile("spotify.txt"):
        return

    client_id = ""
    client_secret = ""
    with open("spotify.txt", "r") as f:
        lines = f.readlines()
        client_id = lines[0].strip()
        client_secret = lines[1].strip()

    auth_url = "https://accounts.spotify.com/api/token"
    credentials = base64.b64encode("{}:{}".format(client_id, client_secret).encode('utf-8')).decode('utf-8')
    headers = { "Authorization": "Basic " + credentials }
    data = { "grant_type": "client_credentials" }
    r = requests.post(auth_url, headers=headers, data=data)
    if r.status_code != 200:
        print("Spotify API authorisation failed!", file=sys.stderr)
        print(r.json())
        if input("Continue with slow web scraping instead? [y/N] ") != "y":
            sys.exit()
        return

    spotify_auth_header = { "Authorization": "Bearer " + r.json().get("access_token") }
    print("Successfully requested Spotify API Token.")

# returns the duration (in seconds) of a spotify track
def get_spotify_track_duration_from_website(track_uri: str) -> Optional[int]:
    try:
        # **Insert meme of virgin API user vs chad web scraper**
        track_url = spotify_track_url(track_uri)
        r = requests.get(track_url)
        if r.status_code != requests.codes.ok:
            print("{} returned status_code {}".format(track_url, r.status_code), file=sys.stderr)
            print(r.text, file=sys.stderr)
            return None

        # info is before this button
        button_identifier = 'aria-label="Save to Your Library"'
        if button_identifier not in r.text:
            print(r.text, file=sys.stderr)
            print("`{}` is not in the HTML response!".format(button_identifier), file=sys.stderr)
            return None

        text = r.text.rsplit(button_identifier, maxsplit=1)[0]
        after_duration_identifier = "</span></div></div><div "
        if after_duration_identifier not in text:
            print(text, file=sys.stderr)
            print("`{}` is not in the HTML response!".format(after_duration_identifier), file=sys.stderr)
            return None

        text = text.rsplit(after_duration_identifier, maxsplit=1)[0]
        if ">" not in text:
            print(text, file=sys.stderr)
            print("`>` is not in the HTML response!", file=sys.stderr)
            return None

        duration = text.rsplit(">", maxsplit=1)[1]

        if not ":" in duration:
            print(duration, file=sys.stderr)
            print("There is no `:` here. This is probably not the duration!", file=sys.stderr)
            return None

        hours = "0"
        minutes, seconds = duration.rsplit(":", maxsplit=1)
        if ":" in minutes:
            hours, minutes = minutes.split(":", maxsplit=1)

        try:
            hours, minutes, seconds = int(hours), int(minutes), int(seconds)
        except ValueError:
            print("Could not convert the duration `{}`!".format(duration), file=sys.stderr)
            return None

        duration_seconds = 3600 * hours + 60 * minutes + seconds

        return duration_seconds
    except Exception as e:
        print("Error encountered!", file=sys.stderr)
        print(e, file=sys.stderr)
        return None

track_duration_cache: dict[str, int] = dict()
if os.path.isfile(duration_cache_path):
    with open(duration_cache_path, "rb") as f:
        track_duration_cache = pickle.load(f)

# returns the duration (in seconds) of a spotify track
# if available the Spotify API is used
def get_spotify_track_duration(track_uri: str) -> Optional[int]:
    global track_duration_cache
    global pause_spotify_api_until

    duration_seconds = None

    try:
        # minimise HTML requests by caching results
        if track_uri in track_duration_cache:
            return track_duration_cache[track_uri]

        if spotify_auth_header is not None and pause_spotify_api_until < datetime.now(tz=timezone.utc):
            r = requests.get("https://api.spotify.com/v1/tracks/{}".format(track_uri.rsplit(":", maxsplit=1)[1]), headers=spotify_auth_header)
            if r.status_code != 200:
                print("Error in Spotify API request!")
                print("Status code:", r.status_code)
                print(r.text)
                print("Resorting to web scraping instead...")
                if r.status_code == 429:
                    # too many requests
                    # wait a while
                    seconds_to_wait = 100
                    try:
                        answer_headers = r.json()
                        seconds_to_wait = answer_headers.get("Retry-After", 100)
                    except Exception:
                        pass
                    pause_spotify_api_until = datetime.now(tz=timezone.utc) + timedelta(seconds=seconds_to_wait)
                elif r.status_code in [419, 498, 440]:
                    # token expired
                    set_spotify_api_auth_header()
            else:
                duration_seconds = r.json()["duration_ms"] // 1000

        if duration_seconds is None:
            duration_seconds = get_spotify_track_duration_from_website(track_uri)

        # cache the result for the future
        if duration_seconds is not None:
            track_duration_cache[track_uri] = duration_seconds
            with open(duration_cache_path, "wb") as f:
                pickle.dump(track_duration_cache, f, pickle.HIGHEST_PROTOCOL)
    except Exception as e:
        print("Error in getting track duration!", file=sys.stderr)
        print(e)

    return duration_seconds

# a summary of the listenbrainz payload
def lb_listen_summary(listen) -> str:
    return "{}\t{} - {}".format(datetime.fromtimestamp(listen["listened_at"], tz=timezone.utc).isoformat(), listen["track_metadata"]["artist_name"], listen["track_metadata"]["track_name"])

# a summary of the spotify listen json object
def spotify_listen_summary(json_path: str, spotify_listen) -> str:
    return "{}:{} {} - {}\t{}".format(json_path, spotify_listen["ts"], spotify_listen["master_metadata_album_artist_name"], spotify_listen["master_metadata_track_name"], spotify_listen["spotify_track_uri"])

def write_to_not_imported_path(spotify_listen):
    first_entry = False
    if not os.path.isfile(not_imported_path):
        first_entry = True
        with open(not_imported_path, "w") as f:
            f.write("[")
    with open(not_imported_path, "a") as f:
        if not first_entry:
            f.write(",")
        f.write("\n")
        f.write(json.dumps(spotify_listen, indent=4, ensure_ascii=False))

def close_not_imported_path():
    if os.path.isfile(not_imported_path):
        with open(not_imported_path, "a") as f:
            f.write("\n]\n")

def json_spotify_listens_to_listenbrainz_payload(json_path: str, spotify_listens=None, min_date=MIN_DATE, max_date=datetime.now(tz=timezone.utc), lbexport=dict()):
    global current_file_spotify_listen_counter
    global duration_not_found_counter
    global rejected_for_duration_counter
    global skipped_because_of_date_counter
    global skipped_because_of_duplicate_counter
    global skipped_because_of_error_counter
    global skipped_because_of_missing_key_counter
    global spotify_listen_counter
    global stop_early
    current_file_spotify_listen_counter = 0

    def check_key(json_element, key: str) -> bool:
        if key not in json_element or json_element[key] is None:
            return False
        return True

    if spotify_listens is None:
        spotify_listens = json.load(open(json_path, "r"))

    for spotify_listen in spotify_listens:
        spotify_listen_counter += 1
        current_file_spotify_listen_counter += 1

        if any(not check_key(spotify_listen, key) for key in ["ts", "master_metadata_album_artist_name", "master_metadata_track_name", "ms_played", "spotify_track_uri", "offline", "offline_timestamp"]):
            skipped_because_of_missing_key_counter += 1
            continue

        try:
            offline = spotify_listen["offline"]
            offline_timestamp = spotify_listen["offline_timestamp"] // 1000
            ts = datetime.fromisoformat(spotify_listen["ts"])
            # if it was played in offline mode then prefer the offline timestamp
            if offline and offline_timestamp >= MIN_DATE_TS:
                ts = datetime.fromtimestamp(offline_timestamp, tz=timezone.utc)
            if ts < min_date or ts > max_date:
                skipped_because_of_date_counter += 1
                continue
            listened_at = int(ts.timestamp())
            track_uri = spotify_listen["spotify_track_uri"]
            ms_played = spotify_listen["ms_played"]
            artist = spotify_listen["master_metadata_album_artist_name"]
            track = spotify_listen["master_metadata_track_name"]
            album = spotify_listen.get("master_metadata_album_album_name")
            track_url = spotify_track_url(track_uri)

            if ms_played < 4 * 60 * 1000:
                # we listened for less than 4 mintues
                # => we need to check if it was at least half the song's duration
                track_duration = get_spotify_track_duration(track_uri)

                if track_duration is None:
                    print("Could not get track duration of {}".format(spotify_listen_summary(json_path, spotify_listen)), file=sys.stderr)
                    write_to_not_imported_path(spotify_listen)
                    duration_not_found_counter += 1
                    continue

                # neither 4 minutes of playtime, nor played more than half the duration?
                if ms_played < track_duration * 500:
                    # => do not scrobble this short listen
                    rejected_for_duration_counter += 1
                    continue

            if listened_at in lbexport:
                matches = []
                for potential_match in lbexport[listened_at]:
                    track_metadata = potential_match["track_metadata"]
                    if "additional_info" not in track_metadata:
                        continue
                    additional_info = track_metadata["additional_info"]
                    if "spotify_id" not in additional_info:
                        continue
                    if "duration_ms" not in additional_info:
                        continue
                    if "submission_client" not in additional_info:
                        continue
                    if additional_info["submission_client"] != "spotify-to-listenbrainz-history-import":
                        continue
                    if additional_info["duration_ms"] != ms_played:
                        continue
                    if additional_info["spotify_id"] != track_url:
                        continue
                    matches.append(potential_match)
                if len(matches) != 0:
                    skipped_because_of_duplicate_counter += 1
                    continue

            payload = {
                    "listened_at": listened_at,
                    "track_metadata": {
                        "artist_name": artist,
                        "track_name": track,
                        },
                    }

            payload["track_metadata"]["additional_info"] = {
                    "artist_names": [artist],
                    "duration_ms": ms_played,
                    "music_service": "spotify.com",
                    "origin_url": track_url,
                    "release_artist_name": artist,
                    "release_artist_names": [artist],
                    "spotify_id": track_url,
                    "submission_client": "spotify-to-listenbrainz-history-import"
                    }

            if album is not None:
                payload["track_metadata"]["release_name"] = album

            yield payload
        except Exception as e:
            print("Error encountered for {}".format(spotify_listen_summary(json_path, spotify_listen)), file=sys.stderr)
            print(spotify_listen, file=sys.stderr)
            print(e, file=sys.stderr)
            write_to_not_imported_path(spotify_listen)
            while True:
                yn = input("Do you want to skip this element and continue with the next one? [y/n] ")
                if yn.lower() in ["y", "yes"]:
                    skipped_because_of_error_counter += 1
                    break
                elif yn.lower() in ["n", "no"]:
                    stop_early = True
                    return None
                else:
                    print("Invalid input! Type y or n for \"yes\" or \"no\" respectively.", file=sys.stderr)
                    continue

def first_track_debugging(lb_listen, auth_header: dict[str, str], username: str):
    print("This is what the first valid track looks like:")
    print(lb_listen_summary(lb_listen))
    print(json.dumps(lb_listen, indent=4, ensure_ascii=False))
    if input("Does everything look correct to you? [y/n] ") != "y":
        sys.exit()

    print("Setting first track as \"Listening now\" for testing purposes.")
    lb_listen_without_listened_at = lb_listen.copy()
    del lb_listen_without_listened_at["listened_at"]
    post_payload = {"listen_type": "playing_now", "payload": [lb_listen_without_listened_at]}
    r = requests.post("{}/1/submit-listens".format(LB_API_URL), json=post_payload, headers=auth_header)
    if r.status_code != 200:
        print("Could not set first track as \"Listening now\"", file=sys.stderr)
        print(r.json(), file=sys.stderr)
        sys.exit(1)

    print("The first track has now been set to to \"Listening now\".")
    print("Go to https://listenbrainz.org/user/{}/ and check if it looks correct!".format(username))
    if input("Importing history to the ListenBrainz user {} now. Ready? [y/N] ".format(username)) != "y":
        sys.exit()

def test_batch_debugging(listens, username: str):
    print("The first {} entries have now been scrobbled.".format(len(listens)))
    print("\n".join([lb_listen_summary(listen) for listen in listens]))
    min_ts = min([listen["listened_at"] for listen in listens])
    print("Please check if they look good!\nhttps://listenbrainz.org/user/{}/?min_ts={}".format(username, min_ts - 1))
    if input("Continue? [y/N] ") != "y":
        sys.exit()

def main():
    if os.path.exists(not_imported_path):
        print("{} already exists!".format(not_imported_path))
        sys.exit(1)

    arg_lb_token = None
    min_date = MIN_DATE
    max_date = datetime.now(tz=timezone.utc)
    dry_run = False
    json_files = []
    lbexport_path = None
    i = 1
    while i < len(sys.argv):
        if sys.argv[i] == "--lb-token":
            arg_lb_token = sys.argv[i + 1]
            i += 2
        elif sys.argv[i] == "--min-date":
            min_date = datetime.fromisoformat(sys.argv[i + 1]).replace(tzinfo=timezone.utc)
            i += 2
        elif sys.argv[i] == "--max-date":
            max_date = datetime.fromisoformat(sys.argv[i + 1]).replace(tzinfo=timezone.utc)
            i += 2
        elif sys.argv[i] == "--lbexport":
            lbexport_path = sys.argv[i + 1]
            i += 2
        elif sys.argv[i] == "--dry-run" or sys.argv[i] == "-n":
            dry_run = True
            i += 1
        else:
            json_files.append(sys.argv[i])
            i += 1

    if "-h" in sys.argv[1:] or "--help" in sys.argv[1:] or len(json_files) == 0 or arg_lb_token is None:
        print("Usage: {} --lb-token <token> json-files...".format(sys.argv[0]), file=sys.stderr)
        print("Optional arguments:")
        print("-n, --dry-run\t\tDo not upload to ListenBrainz. Useful for making sure there are no errors before uploading.")
        print("--min-date <DATE>\tSkip all entries where the UTC timestamp is before <DATE> (in ISO 8601 format)")
        print("--max-date <DATE>\tSkip all entries where the UTC timestamp is after <DATE> (in ISO 8601 format)")
        print("--lbexport <FILE>\tAvoid submitting duplicates by checking against the ListenBrainz export from https://listenbrainz.org/profile/export/")
        print("\t\t\tOnly detects duplicates from submissions that were uploaded with this script!")
        print()
        print("Get the token at https://listenbrainz.org/profile/", file=sys.stderr)
        print("Get the json-files with the listening history from Spotify at https://www.spotify.com/account/privacy/", file=sys.stderr)
        sys.exit(1)

    lbexport = dict()
    if lbexport_path is not None:
        for lb_entry in json.load(open(lbexport_path, "rb")):
            if lb_entry["listened_at"] not in lbexport:
                lbexport[lb_entry["listened_at"]] = []
            lbexport[lb_entry["listened_at"]].append(lb_entry)

    lb_auth_header = {
        "Authorization": "Token {}".format(arg_lb_token)
    }
    r = requests.get("{}/1/validate-token".format(LB_API_URL), headers=lb_auth_header)
    token_validation = r.json()
    if r.status_code != 200:
        print("Your provided token is invalid!", file=sys.stderr)
        print(token_validation, file=sys.stderr)
        sys.exit(1)
    username = token_validation["user_name"]
    print("ListenBrainz token of user {} provided.".format(username))

    set_spotify_api_auth_header()

    # start the import
    max_listens_per_request = 1000
    uploaded_counter = 0
    test_batch = True
    timer = datetime.now(tz=timezone.utc)
    for json_file in json_files:
        print(json_file)
        json_load = json.load(open(json_file, "r"))
        lb_listens_iter = json_spotify_listens_to_listenbrainz_payload(json_file, json_load, min_date, max_date, lbexport)
        while True:
            n = max_listens_per_request
            if test_batch:
                n = 10
            lb_listens = list(islice(lb_listens_iter, n))
            if len(lb_listens) == 0:
                # done with this file
                break

            # inspect the first track to make sure everything is alright
            if test_batch:
                first_track_debugging(lb_listens[0], lb_auth_header, username)

            if stop_early and len(lb_listens) > 0:
                if input("Do you still want to send the last {} valid tracks that we fetched? [y/N] ".format(len(lb_listens))) != "y":
                    break

            post_payload = {"listen_type": "import", "payload": lb_listens}
            if not dry_run:
                while True:
                    r = requests.post("{}/1/submit-listens".format(LB_API_URL), json=post_payload, headers=lb_auth_header)
                    if r.status_code == 429:
                        wait_s = int(r.headers["X-RateLimit-Reset-In"])
                        print("ListenBrainz rate limit surpassed. Waiting {} s.".format(wait_s))
                        time.sleep(wait_s)
                        continue
                    if r.status_code != 200:
                        print("Error sending payload:", file=sys.stderr)
                        print(post_payload, file=sys.stderr)
                        print("Answer:", file=sys.stderr)
                        print(r.json(), file=sys.stderr)
                        print("Status code: {}".format(r.status_code), file=sys.stderr)
                        close_not_imported_path()
                        raise Exception("Stopping before {}: {}".format(json_file, lb_listen_summary(lb_listens[0])))
                    break
            uploaded_counter += len(lb_listens)

            if test_batch:
                test_batch_debugging(lb_listens, username)
                timer = datetime.now(tz=timezone.utc)
                test_batch = False

            global current_file_spotify_listen_counter
            print("{}\t{}/{}".format(json_file, current_file_spotify_listen_counter, len(json_load)))
            print("Last upload: {}".format(lb_listen_summary(lb_listens[-1])))

            if stop_early:
                break

        current_file_spotify_listen_counter = 0
        if stop_early:
            break

    close_not_imported_path()

    print("\nDONE after {}.".format(datetime.now(tz=timezone.utc) - timer))
    print("Here are some statistics.\n")
    print("Total elements in json files: {}".format(spotify_listen_counter))
    print("Uploaded: {}".format(uploaded_counter))
    print("Rejected, because of short duration: {}".format(rejected_for_duration_counter))
    print("Skipped, because the timestamp was not in our date range: {}".format(skipped_because_of_date_counter))
    print("Skipped, because of missing key: {}".format(skipped_because_of_missing_key_counter))
    print("Skipped, because we could not find the duration: {}".format(duration_not_found_counter))
    print("Skipped, because of errors: {}".format(skipped_because_of_error_counter))
    if lbexport_path is not None:
        print("Skipped, because of duplicates: {}".format(skipped_because_of_duplicate_counter))

    if duration_not_found_counter != 0 or skipped_because_of_error_counter != 0:
        print("\nThe entries that were skipped, because we could not find the duration or because there were errors, are in {}.".format(not_imported_path))

if __name__ == "__main__":
    main()
